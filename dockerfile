FROM ubuntu:latest
Maintainer Yashwin Reddy (yashwin012@gmail.com)
RUN apt-get update -y
RUN apt upgrade -y
cmd ["date"]
run apt install git -y
run git clone https://gitlab.com/xemesis3/tyrannasaurus.git
run sleep 60
run apt install apache2 -y
cmd ["service apache2 start"]
# run rm /usr/share/nginx/html/index.html
WORKDIR "cd /tyrannasaurus/"
CMD ["pwd"]
# run sleep 10
# CMD ["date"]
run cp /tyrannasaurus/index.html /var/www/html/index.html
# entrypoint ["/usr/sbin/nginx","-g","daemon off;"]
# expose 80/tcp
# ENTRYPOINT []


# COPY default /etc/nginx/sites-available/default
